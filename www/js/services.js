angular.module('app.services', [])

.factory('BlankFactory', [function(){

}])
.service('LoginService', function($q) {
    return {
        loginUser: function(name, pw) {
            var deferred = $q.defer();
            var promise = deferred.promise;

            // futuro service - sarvar sessão
            if (name == 'tanaral' && pw == '123456') {
                deferred.resolve('agendamentosAdmin');
            } else if (name == 'jorgea' && pw == '123456') {
                deferred.resolve('agendamentosConsultor');
            } else if (name == 'brunob' && pw == '123456') {
                deferred.resolve('agendamentosSolicitante');
            } else {
                deferred.reject('Wrong credentials.');
            }

            promise.success = function(fn) {
                promise.then(fn);
                return promise;
            }
            promise.error = function(fn) {
                promise.then(null, fn);
                return promise;
            }
            return promise;
        }
    }
});
